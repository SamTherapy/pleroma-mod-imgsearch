# Pleroma Mod Imgsearch

This script adds a simple button on image attachments, to allow quick searching for the image source.

## Install

Go to your pleroma-mods directory and run the following command:

```
sudo -u pleroma git clone https://git.froth.zone/Sam/pleroma-mod-imgsearch
```

Now go to your pleroma-mod-loader configuration and add `imgsearch` to enable the mod.

## Configuration

There is no configuration as of yet.
